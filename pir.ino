
int calibrationTime = 60;       // Temps de calibrage en seconde du PIR

int ledPin1 = 13;                // Pin ou se trouve la LED
int inputPin1 = 3;               // Pin ou se trouve le PIR
int pirState1 = LOW;             // 
int val1 = 0;                    // variable : lecture du statut du PIN

int ledPin2 = 12;                // Pin ou se trouve la LED
int inputPin2 = 2;               // Pin ou se trouve le PIR
int pirState2 = LOW;             // 
int val2 = 0;                    // variable : lecture du statut du PIN

void setup() {

    Serial.print("Pir - Hocine.");

    pinMode(ledPin1, OUTPUT);      
    pinMode(inputPin1, INPUT);     
    pinMode(ledPin2, OUTPUT);
    pinMode(inputPin2, INPUT);   
  
    Serial.begin(9600);

    Serial.println("...Calibrage");
    for(int i = 0; i < calibrationTime; i++){
        Serial.print(i);
        delay(1000);
    }
    Serial.println("... Fin du calibrage.");
}

void loop() {

    detect(pirState1, ledPin1, val1, inputPin1, 1);
    detect(pirState2, ledPin2, val2, inputPin2, 2);

}

/**
 * pirState
 * ledPin
 * val
 * inputPin
 * x numero
 */
void detect(int pirState, int ledPin, int val, int inputPin, int x) {

    val = digitalRead(inputPin);
    Serial.println(val);

   if (val == HIGH) {
        
        digitalWrite(ledPin, HIGH);   // LED : ON
        delay(150);
  
        if (pirState == LOW) {
          
          Serial.println("Yo : " + x);
          pirState = HIGH;
          
        }
    
    } else {
      
        digitalWrite(ledPin, LOW);    // LED : OFF
        delay(300);
        
        if (pirState == HIGH) {
          
            Serial.println("Fin : " + x);
            pirState = LOW;
        }
  
    }
 
}



